import { useTheme } from "@emotion/react";
import { Box, Typography } from "@mui/material";
import { shades } from "../../theme";

function Footer() {
  const {
    palette: { neutral },
  } = useTheme();
  return (
    <Box marginTop="70px" padding="40px 0" backgroundColor={neutral.light}>
      <Box
        width="80%"
        margin="auto"
        display="flex"
        justifyContent="space-between"
        flexWrap="wrap"
        rowGap="30px"
        columnGap="clamp(20px, 30px, 40px)"
      >
        <Box width="clamp(20%, 30%, 40%)">
          <Typography
            variant="h4"
            fontWeight="bold"
            mb="30px"
            color={shades.secondary[500]}
          >
            Bottled Nature
          </Typography>
          <div>
            Natures superfood, 100% Organic Virgin coconut oil is a building
            block for healthy hair and & skin for generations. Our range of
            products aims at harnessing nature and bottling it for our
            generation to enjoy the benefits.
          </div>
          <div>
            Hibiscus Oil Hibiscus (Hibiscus Sabdariffa L) is formulated with the
            Organic Virgin Coconut oil, It is natures powerhouse of amino acids,
            which provides your hair with nutrients that promote hair growth.
          </div>
          <div>
            Amino acids are great at producing keratin, a building block of hair
            that binds the hair and prevents breakage. Keratin also promotes the
            thickness of the hair strands, making it more manageable
          </div>
        </Box>

        <Box>
          <Typography variant="h4" fontWeight="bold" mb="30px">
            About Us
          </Typography>
          <Typography mb="30px">Careers</Typography>
          <Typography mb="30px">Our Stores</Typography>
          <Typography mb="30px">Terms & Conditions</Typography>
          <Typography mb="30px">Privacy Policy</Typography>
        </Box>

        <Box>
          <Typography variant="h4" fontWeight="bold" mb="30px">
            Customer Care
          </Typography>
          <Typography mb="30px">Help Center</Typography>
          <Typography mb="30px">Track Your Order</Typography>
          <Typography mb="30px">Corporate & Bulk Purchasing</Typography>
          <Typography mb="30px">Returns & Refunds</Typography>
        </Box>

        <Box width="clamp(20%, 25%, 30%)">
          <Typography variant="h4" fontWeight="bold" mb="30px">
            Contact Us
          </Typography>
          <Typography mb="30px">Doonside, NSW Australia</Typography>
          <Typography mb="30px" sx={{ wordWrap: "break-word" }}>
            Email: bottlednature@gmail.com
          </Typography>
          <Typography mb="30px">(+61)468415857</Typography>
        </Box>
      </Box>
    </Box>
  );
}

export default Footer;
